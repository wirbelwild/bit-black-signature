[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6f901180324a42799ce05558d7d56761)](https://www.codacy.com/manual/wirbelwild/Bit-Black-Signature?utm_source=tobiaskoengeter@bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/bit-black-signature&amp;utm_campaign=Badge_Grade)

# Bit&Black Signature

Displays the Bit&Black signature in the CLI.

## Installation 

This library is made for the use with NPM. Add it to your project by running `$ npm install bitandblack-signature` or `$ yarn add bitandblack-signature`.

## Usage 

Import the function and run it like that:

````javascript
import Signature from "~signature";

new Signature();
````

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.