/**
 * Displays the Bit&Black message.
 */
function Signature() 
{
    console.log(
        "%c Made with love by Bit&Black ",
        "font-weight: bolder; font-size: 32px; line-height: 1.4; color: #000; text-shadow: 3px 3px 0 #d23cea, 6px 6px 0 #568eb4, 9px 9px 0 #5fb456, 12px 12px 0 #d9ab1a, 15px 15px 0 #d9461a"
    );
}

/**
 * Displays the coffee message.
 */
function OpenSourceCoffee() 
{
    console.log("Are you an open source developer? We support your work with a free cup of coffee! \nVisit as in our office at Überkinger Straße 4, 70372 Stuttgart, Germany. https://goo.gl/maps/oaqhSWJzFRmYSSKKA \nOf course, you can also get a tea or good water from the mineral springs in Bad Cannstatt. \nWe look forward to getting to know you and exchanging ideas with you. \n#SupportOpenSource ");
}

export { Signature, OpenSourceCoffee };